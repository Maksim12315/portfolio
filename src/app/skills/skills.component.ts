import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  data = [
    {
      "name": "ES6",
      "value": 42
    },
    {
      "name": "Angular 4",
      "value": 56
    },
    {
      "name": "Webpack",
      "value": 35
    },
    {
      "name": "rxjs",
      "value": 40
    },
    {
      "name": "npm",
      "value": 47
    },
    {
      "name": "TypeScript",
      "value": 61
    }
  ];

  view: any[] = [700, 400];

  constructor() { }

  ngOnInit() {
  }

}
